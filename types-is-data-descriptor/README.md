# Installation
> `npm install --save @types/is-data-descriptor`

# Summary
This package contains type definitions for is-data-descriptor (https://github.com/jonschlinkert/is-data-descriptor).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/is-data-descriptor.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/is-data-descriptor/index.d.ts)
````ts
// Type definitions for is-data-descriptor 1.0
// Project: https://github.com/jonschlinkert/is-data-descriptor
// Definitions by: Richie Bendall <https://github.com/Richienb>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare function isDataDesc(obj: unknown): obj is PropertyDescriptor;
declare function isDataDesc<ObjectType extends object>(obj: ObjectType, key: keyof ObjectType): boolean;

export = isDataDesc;

````

### Additional Details
 * Last updated: Mon, 01 Nov 2021 00:01:22 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Richie Bendall](https://github.com/Richienb).
